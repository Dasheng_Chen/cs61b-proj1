"This project involves writing a miniature relational database management system (DBMS) that stores tables of data, where a table consists of some number of labeled columns of information. Our system will include a very simple query language for extracting information from these tables. "

https://inst.eecs.berkeley.edu/~cs61b/fa14/hw/proj1.pdf