package db61b;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

/**
 * Basic function test for class Row, Table
 * @author Dasheng Chen
 *
 */
public class BasicFunctionTest {

    @Test
    public void testRow() {
        String[] dummy = new String[]{"Hello", "world", "!"};
        Row myRow = new Row(dummy);
        assertEquals(dummy.length, myRow.size());
        assertEquals(dummy[1], myRow.get(1));
        Row newRow = new Row(dummy);
        assertEquals(true, myRow.equals(newRow));
        assertEquals("Hello world !\n", newRow.toString());
        String[] data = new String[]{"101", "21228", "B"};
        Row anotherRow = new Row(data);
        assertEquals("101 21228 B\n", anotherRow.toString());
        Table enrolled = Table.readTable("./testing/enrolled");
        Table students = Table.readTable("./testing/students");
        ArrayList<Column> colList = new ArrayList<Column>();
        colList.add(new Column("SID", enrolled, students));
        colList.add(new Column("Lastname", enrolled, students));
        colList.add(new Column("Firstname", enrolled, students));
        Row r1 = enrolled.iterator().next();
        Row r2 = students.iterator().next();
        Row r3 = new Row(colList, r1, r2);
        Row r4 = new Row(colList, r1, r2);
        System.out.println();
        assertEquals(true, r3.equals(r4));
        assertEquals(3, r3.size());
        assertEquals(r3.get(0), r4.get(0));
        assertEquals(r3.get(1), r4.get(1));
        assertEquals(r3.get(2), r4.get(2));
        assertEquals(r3.toString(), r4.toString());
    }

    @Test
    public void testColumn() {
        Table enrolled = Table.readTable("./testing/enrolled");
        Column ccn = new Column("CCN", enrolled);
        Column sid = new Column("SID", enrolled);
        String[] s = new String[3];
        s[0] = "101";
        s[1] = "21228";
        s[2] = "B";
        Row row = new Row(s);
        assertEquals("21228", ccn.getFrom(row));
        assertEquals("CCN", ccn.getName());
        assertEquals("101", sid.getFrom(row));
        assertEquals("SID", sid.getName());
    }

    @Test
    public void testTable() {
        String[] columnTitle = new String[]{"ID", "name", "class"};
        Table myTable = new Table(columnTitle);
        ArrayList<String> columnList = new ArrayList<String>();
        assertEquals(columnTitle.length, myTable.columns());
        for (int i = 0; i < columnTitle.length; i += 1) {
            columnList.add(columnTitle[i]);
            assertEquals(columnTitle[i], myTable.getTitle(i));
        }
        assertEquals(0, myTable.size());
        assertEquals(columnTitle.length, myTable.getColumn().size());
        ArrayList<Column> alColumn = myTable.getColumn();
        for (int i = 0; i < columnTitle.length; i += 1) {
            assertEquals(columnTitle[i], alColumn.get(i).getName());
        }
        assertEquals(1, myTable.findColumn("name"));
        assertEquals(true,
            myTable.add(new Row(new String[] { "1", "Dasheng", "cs61b" })));
        assertEquals(false,
            myTable.add(new Row(new String[] { "1", "Dasheng", "cs61b" })));
        Table newTable = Table.readTable("./testing/enrolled");
        newTable.writeTable("testWriteTableEnrolled");
        newTable.print();
        Column col1 = new Column("ID", myTable);
        Condition c = new Condition(col1, "=", "1");
        ArrayList<Condition> al = new ArrayList<Condition>();
        al.add(c);
        myTable.select(columnList, al);
        for (Row r : myTable) {
            assertEquals("1 Dasheng cs61b\n", r.toString());
        }
        String[] table2Col = new String[]{"ID", "Height", "Eye"};
        Table table2 = new Table(table2Col);
        table2.add(new Row(new String[] { "3", "177", "red"}));
        table2.add(new Row(new String[] { "2", "167", "black"}));
        col1 = new Column("ID", myTable, table2);
        Column col2 = new Column("ID", myTable, table2);
        ArrayList<Condition> newCondList = new ArrayList<Condition>();
        Condition newCond = new Condition(col1, "=", col2);
        newCondList.add(newCond);
        columnList.clear();
        columnList.add("ID");
        columnList.add("name");
        Table t = myTable.select(table2, columnList, newCondList);
        for (Row r : t) {
            assertEquals("", r.toString());
        }
        table2.add(new Row(new String[] { "1", "127", "black"}));
        t = myTable.select(table2, columnList, newCondList);
        for (Row r : t) {
            assertEquals("1 Dasheng\n", r.toString());
        }
    }

    @Test
    public void testEquijoin() {
        Table t1 = Table.readTable("./testing/t1");
        Table t2 = Table.readTable("./testing/t2");
        ArrayList<Column> common1 = Table.commonColumnList(t1.getColumn(),
            t2.getColumn());
        ArrayList<Column> common2 = Table.commonColumnList(t2.getColumn(),
            t1.getColumn());
        Iterator<Row> iter1 = t1.iterator();
        Iterator<Row> iter2 = t2.iterator();
        Row r1 = iter1.next();
        Row r2 = iter2.next();
        System.out.println(r1.toString());
        System.out.println(r2.toString());
        assertEquals(false, Table.equijoin(common1, common2, r1, r2));
        r1 = iter1.next();
        r2 = iter2.next();
        System.out.println(r1.toString());
        System.out.println(r2.toString());
        assertEquals(true, Table.equijoin(common1, common2, r1, r2));
    }

    @Test
    public void testCommonColumnList() {
        Table t1 = Table.readTable("./testing/t1");
        Table t2 = Table.readTable("./testing/t2");
        ArrayList<Column> common = Table.commonColumnList(t1.getColumn(),
            t2.getColumn());
        assertEquals(2, common.size());
        common = Table.commonColumnList(t2.getColumn(), t1.getColumn());
        assertEquals(2, common.size());
    }

    @Test
    public void testDatabase() {
        String[] columnTitle = new String[]{"ID", "name", "class"};
        Table myTable = new Table(columnTitle);
        Database db = new Database();
        assertEquals(null, db.get("enroll"));
        db.put("enroll", myTable);
        assertEquals(myTable, db.get("enroll"));
    }

    @Test
    public void testConditionWithLiteral() {
        Table table = Table.readTable("./testing/enrolled");
        Column c = new Column("CCN", table);
        String ccn = "107";
        Condition cond = new Condition(c, "!=", ccn);
        ArrayList<Condition> al = new ArrayList<Condition>();
        al.add(cond);
        Iterator<Row> iter = table.iterator();
        while (iter.hasNext()) {
            assertEquals(true, Condition.test(al, iter.next()));
        }
        ccn = "100";
        cond = new Condition(c, ">", ccn);
        al.clear();
        al.add(cond);
        iter = table.iterator();
        while (iter.hasNext()) {
            assertEquals(true, Condition.test(al, iter.next()));
        }
        cond = new Condition(c, ">=", ccn);
        al.clear();
        al.add(cond);
        iter = table.iterator();
        while (iter.hasNext()) {
            assertEquals(true, Condition.test(al, iter.next()));
        }
        cond = new Condition(c, "<", ccn);
        al.clear();
        al.add(cond);
        iter = table.iterator();
        while (iter.hasNext()) {
            assertEquals(false, Condition.test(al, iter.next()));
        }
        cond = new Condition(c, "<=", ccn);
        al.clear();
        al.add(cond);
        iter = table.iterator();
        while (iter.hasNext()) {
            assertEquals(false, Condition.test(al, iter.next()));
        }
    }

    @Test
    public void testCondition() {
        Table table = Table.readTable("./testing/enrolled");
        Column c = new Column("CCN", table);
        Column sid = new Column("SID", table);
        ArrayList<Condition> al = new ArrayList<Condition>();
        Condition cond = new Condition(sid, "!=", c);
        al.clear();
        al.add(cond);
        String[] s1 = new String[3];
        s1[0] = "101";
        s1[1] = "21228";
        s1[2] = "B";
        Row row1 = new Row(s1);
        assertEquals(true, Condition.test(al, row1));
        cond = new Condition(sid, "!=", sid);
        String[] s2 = new String[3];
        s2[0] = "101";
        s2[1] = "21228";
        s2[2] = "B";
        Row row2 = new Row(s2);
        al.clear();
        al.add(cond);
        assertEquals(false, Condition.test(al, row1, row2));
        al.clear();
        al.add(new Condition(sid, ">=", sid));
        al.add(new Condition(sid, "<=", sid));
        al.add(new Condition(sid, "=", sid));
        assertEquals(true, Condition.test(al, row1, row2));
    }

    @Test
    public void testCondition2() {
        Table table = Table.readTable("./testing/enrolled");
        Column sid = new Column("SID", table);
        Column sid2 = new Column("CCN", table);
        sid = new Column("SID", table);
        String[] s3 = new String[3];
        s3[0] = "102";
        s3[1] = "21001";
        s3[2] = "B+";
        Row row3 = new Row(s3);
        String[] s1 = new String[3];
        s1[0] = "101";
        s1[1] = "21228";
        s1[2] = "B";
        Row row1 = new Row(s1);
        ArrayList<Condition> al = new ArrayList<Condition>();
        al.add(new Condition(sid, ">", sid2));
        assertEquals(false, Condition.test(al, row1, row3));
        al.clear();
        al.add(new Condition(sid2, ">", sid));
        assertEquals(true, Condition.test(al, row3, row1));
        al.clear();
        al.add(new Condition(sid, "<", sid2));
        assertEquals(true, Condition.test(al, row1, row3));
        al.clear();
        al.add(new Condition(sid2, "<", sid));
        assertEquals(false, Condition.test(al, row3, row1));
    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(BasicFunctionTest.class));
    }
}
