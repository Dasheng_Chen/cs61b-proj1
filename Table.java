package db61b;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static db61b.Utils.*;

/** A single table in a database.
 *  @author P. N. Hilfinger
 */
class Table implements Iterable<Row> {
    /** A new Table whose columns are given by COLUMNTITLES, which may
     *  not contain dupliace names. */
    Table(String[] columnTitles) {
        for (int i = columnTitles.length - 1; i >= 1; i -= 1) {
            for (int j = i - 1; j >= 0; j -= 1) {
                if (columnTitles[i].equals(columnTitles[j])) {
                    throw error("duplicate column name: %s",
                                columnTitles[i]);
                }
            }
        }
        this._columnTitle = new String[columnTitles.length];
        for (int i = 0; i < columnTitles.length; i += 1) {
            this._columnTitle[i] = columnTitles[i];
        }
    }

    /** A new Table whose columns are give by COLUMNTITLES. */
    Table(List<String> columnTitles) {
        this(columnTitles.toArray(new String[columnTitles.size()]));
    }

    /** Return the number of columns in this table. */
    public int columns() {
        return _columnTitle.length;
    }

    /** Return the title of the Kth column.  Requires 0 <= K < columns(). */
    public String getTitle(int k) {
        if (k < 0 || k >= columns()) {
            throw error("k is out of range.");
        }
        return _columnTitle[k];
    }

    /** Return the number of the column whose title is TITLE, or -1 if
     *  there isn't one. */
    public int findColumn(String title) {
        for (int i = 0; i < columns(); i += 1) {
            if (title.equals(_columnTitle[i])) {
                return i;
            }
        }
        return -1;
    }

    /** Return the number of Rows in this table. */
    public int size() {
        return _rows.size();
    }

    /** Returns an iterator that returns my rows in an unspecfied order. */
    @Override
    public Iterator<Row> iterator() {
        return _rows.iterator();
    }

    /** Add ROW to THIS if no equal row already exists.  Return true if anything
     *  was added, false otherwise. */
    public boolean add(Row row) {
        if (_rows.contains(row)) {
            return false;
        }
        if (row.size() != this.columns()) {
            throw error("number of column is not matched.");
        }
        _rows.add(row);
        return true;
    }

    /** Read the contents of the file NAME.db, and return as a Table.
     *  Format errors in the .db file cause a DBException. */
    static Table readTable(String name) {
        BufferedReader input;
        Table table;
        input = null;
        table = null;
        try {
            input = new BufferedReader(new FileReader(name + ".db"));
            String header = input.readLine();
            if (header == null) {
                throw error("missing header in DB file");
            }
            String[] columnNames = header.split(",");
            table = new Table(columnNames);
            String line = "";
            while ((line = input.readLine()) != null) {
                String[] rowData = line.split(",");
                table.add(new Row(rowData));
            }
        } catch (FileNotFoundException e) {
            throw error("could not find %s.db", name);
        } catch (IOException e) {
            throw error("problem reading from %s.db", name);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    /* Ignore IOException */
                }
            }
        }
        return table;
    }

    /** Write the contents of TABLE into the file NAME.db. Any I/O errors
     *  cause a DBException. */
    void writeTable(String name) {
        PrintStream output;
        output = null;
        try {
            String sep;
            sep = "";
            output = new PrintStream(name + ".db");
            for (int i = 0; i < _columnTitle.length; i += 1) {
                sep += _columnTitle[i];
                if (i != _columnTitle.length - 1) {
                    sep += ",";
                }
            }
            sep += "\n";
            for (Row r : _rows) {
                sep += r.toStringDelimiter(",");
            }
            output.append(sep);
        } catch (IOException e) {
            throw error("trouble writing to %s.db", name);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /** Print my contents on the standard output. */
    void print() {
        String sep = "";
        for (Row r : _rows) {
            sep += "  " + r.toString();
        }
        System.out.print(sep);
    }

    /** Return a new Table whose columns are COLUMNNAMES, selected from
     *  rows of this table that satisfy CONDITIONS. */
    Table select(List<String> columnNames, List<Condition> conditions) {
        Table result = new Table(columnNames);
        ArrayList<Column> columnList = new ArrayList<Column>();
        for (int i = 0; i < columnNames.size(); i += 1) {
            Column c = new Column(columnNames.get(i), this);
            columnList.add(c);
        }
        Iterator<Row> iterRow = this.iterator();
        while (iterRow.hasNext()) {
            Row next = iterRow.next();
            if (Condition.test(conditions, next)) {
                result.add(new Row(columnList, next));
            }
        }
        return result;
    }

    /** Return a new Table whose columns are COLUMNNAMES, selected
     *  from pairs of rows from this table and from TABLE2 that match
     *  on all columns with identical names and satisfy CONDITIONS. */
    Table select(Table table2, List<String> columnNames,
                 List<Condition> conditions) {
        Table result = new Table(columnNames);
        boolean isCartesian = true;
        ArrayList<Column> col1 = this.getColumn();
        ArrayList<Column> col2 = table2.getColumn();
        ArrayList<Column> common1 = Table.commonColumnList(col1, col2);
        ArrayList<Column> common2 = Table.commonColumnList(col2, col1);
        ArrayList<Column> col3 = new ArrayList<Column>();
        for (String colName : columnNames) {
            col3.add(new Column(colName, this, table2));
        }
        for (int i = 0; i < common1.size(); i += 1) {
            for (int j = 0; j < common2.size(); j += 1) {
                if (common1.get(i).getName().equals(common2.get(j).getName())) {
                    isCartesian = false;
                    break;
                }
            }
            if (!isCartesian) {
                break;
            }
        }
        if (isCartesian) {
            for (Row row1 : this) {
                for (Row row2 : table2) {
                    if (Condition.test(conditions, row1, row2)) {
                        result.add(new Row(col3, row1, row2));
                    }
                }
            }
        } else {
            for (Row row1 : this) {
                for (Row row2 : table2) {
                    if (equijoin(common1, common2, row1, row2)
                        && (Condition.test(conditions, row1, row2))) {
                        result.add(new Row(col3, row1, row2));
                    }
                }
            }
        }
        return result;
    }

    /** Return an ArrayList of common column for col1 with col2.
     * @param col1 First column list
     * @param col2 second column list
     * @return ArrayList of common column for col1 with col2
     */
    public static ArrayList<Column> commonColumnList(ArrayList<Column> col1,
        ArrayList<Column> col2) {
        ArrayList<Column> result = new ArrayList<Column>();
        for (int i = 0; i < col1.size(); i += 1) {
            for (int j = 0; j < col2.size(); j += 1) {
                Column c1 = col1.get(i);
                Column c2 = col2.get(j);
                String col1Name = c1.getName();
                String col2Name = c2.getName();
                if (col1Name.equals(col2Name)) {
                    result.add(c1);
                }
            }
        }
        return result;
    }

    /** Return the all the column in an ArrayList. */
    public ArrayList<Column> getColumn() {
        ArrayList<Column> al = new ArrayList<Column>();
        for (int i = 0; i < this._columnTitle.length; i += 1) {
            al.add(new Column(this._columnTitle[i], this));
        }
        return al;
    }

    /** Return true if the columns COMMON1 from ROW1 and COMMON2 from
     *  ROW2 all have identical values.  Assumes that COMMON1 and
     *  COMMON2 have the same number of elements and the same names,
     *  that the columns in COMMON1 apply to this table, those in
     *  COMMON2 to another, and that ROW1 and ROW2 come, respectively,
     *  from those tables. */
    public static boolean equijoin(List<Column> common1, List<Column> common2,
        Row row1, Row row2) {
        int correctTimes = common1.size();
        for (int i = 0; i < common1.size(); i += 1) {
            for (int j = 0; j < common2.size(); j += 1) {
                Column col1 = common1.get(i);
                Column col2 = common2.get(j);
                String col1Name = col1.getName();
                String col2Name = col2.getName();
                if (col1Name.equals(col2Name)
                    && col1.getFrom(row1).equals(col2.getFrom(row2))) {
                    correctTimes -= 1;
                }
            }
        }
        return correctTimes == 0;
    }

    /** My rows. */
    private HashSet<Row> _rows = new HashSet<>();
    /** Column title. */
    private String[] _columnTitle;
}

